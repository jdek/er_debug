# PKG_CONFIG_PATH=../libavcodec:../libswresample:../libavformat:../libavfilter:../libavutil pkgconf --libs
LDFLAGS  = -L../libavcodec -L../libswresample -L../libavformat -L../libavfilter -L../libavutil  -lavcodec -lswresample -lavutil -lavformat
LDFLAGS += -pthread -lm -lz -llzma -lbz2 -liconv
LDFLAGS += -framework AudioToolbox -framework VideoToolbox -framework CoreFoundation -framework CoreMedia -framework CoreVideo -framework CoreServices -framework Security
CFLAGS   = -I..
CFLAGS  += -g

SAMPLES  = /Users/user/Desktop/samples

prog: prog.c
	cc $(CFLAGS) $(LDFLAGS) prog.c -o prog

%.raw: $(SAMPLES)/%.mkv
	./prog $< $@

.PHONY: clean
clean:
	rm -f prog

.PHONY: 420i
420i: chimera_mosh_540i.raw
	mpv --window-scale=0.5 --demuxer=rawvideo --demuxer-rawvideo-w=1024 --demuxer-rawvideo-h=540 --demuxer-rawvideo-fps=48000/1001 --demuxer-rawvideo-format=I420 $<

.PHONY: 420p
420p: chimera_mosh_540p.raw
	mpv --window-scale=0.5 --demuxer=rawvideo --demuxer-rawvideo-w=1024 --demuxer-rawvideo-h=540 --demuxer-rawvideo-fps=24000/1001 --demuxer-rawvideo-format=I420 $<
